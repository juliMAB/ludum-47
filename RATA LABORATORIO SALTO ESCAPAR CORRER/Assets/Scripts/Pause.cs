﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pause : MonoBehaviour
{

    public bool active;
    SpriteRenderer sr = new SpriteRenderer();

    // Start is called before the first frame update
    void Start()
    {
        active = false;
        sr = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.P))
        {
            active = !active;
        }

        sr.enabled = !active;
    }
}
