﻿using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;

public class MoveFloor : MonoBehaviour
{
    public float velocidad;
    public GameObject DeSpawn;
    public GameObject Spawner;
    private Vector3 sss;
    private bool pause;
    // Start is called before the first frame update
    void Start()
    {
        //los pisos van a estar en la misma y que el spawner.
        this.gameObject.transform.SetPositionAndRotation(new Vector3(this.gameObject.transform.position.x, Spawner.transform.position.y, 0), new quaternion());
        //va a sumarse al spawnpoint para que entre desde afuera.
        sss = new Vector3(this.gameObject.transform.localScale.x + transform.localScale.x * 2, 0, 0);
        pause = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.P))
        {
            pause = !pause;
        }

        Time.timeScale = (pause) ? 0 : 1f;

        if (!pause)
        {

            //moverse.
            transform.Translate(Vector3.left * Time.deltaTime * velocidad);
            //llega al Despawn
            if ((transform.position.x + transform.localScale.x * 2) < DeSpawn.transform.position.x)
            {
                //voy a mandar al piso a su  nueva posicion, el spawn.

                //tengo que agregarle el  this gameObjet antes de la coma, suamndolo para agregar el size, pero no se como.
                this.gameObject.transform.SetPositionAndRotation(Spawner.transform.position + sss, new quaternion());
            }
        }
    }
}
