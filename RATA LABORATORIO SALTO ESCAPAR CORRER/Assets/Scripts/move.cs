﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor.UIElements;
using UnityEngine;

public class move : MonoBehaviour
{

    public float velocidad = 1;

    private bool startMoving = false;

    // Update is called once per frame
    void Update()
    {
        movement();
        if (Input.GetKeyDown(KeyCode.P))
            startMoving = true;
    }

    void movement()
    {
        
        if(startMoving)
        transform.Translate(Vector3.left * Time.deltaTime * velocidad);

    }
}
