﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveRata : MonoBehaviour
{

    private Animator animationJump;
    public GameObject rata;
    public bool onFloor;
    public bool bajando;
    public float jumpForce = 100f;
    private bool pause;
    public bool floorTouched;
    public bool correct;

    // Start is called before the first frame update

    private void Awake()
    {

        animationJump = rata.GetComponent<Animator>();
        pause = true;

    }

    void Start()
    {
        onFloor = true;
        bajando = true;
        floorTouched = false;
        correct = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.P))
        {
            pause = !pause;
        }

        animationJump.SetBool("floorTouched", floorTouched);

        Time.timeScale = (pause) ? 0 : 1f;

        if (!pause && floorTouched) {

            animationJump.SetBool("onFloor", onFloor);
            animationJump.SetBool("bajando", bajando);

            if (Input.GetKey(KeyCode.Space) && onFloor)
            {

                GetComponent<Rigidbody2D>().velocity = new Vector2(GetComponent<Rigidbody2D>().velocity.x, jumpForce);
                //animationJump.enabled = true;
                onFloor = false;
                bajando = false;

            }
            if (GetComponent<Rigidbody2D>().velocity.y < 0)
            {
                bajando = true;
                onFloor = false;
            }
            if (transform.position.y < -1 && rata.transform.rotation.z == 0)
            {
                bajando = false;
            }
            if (transform.position.y < -3.2f)
            {
                onFloor = true;
                bajando = false;

            }
        }

        correct = (rata.transform.rotation.z != 0 && onFloor);
        animationJump.SetBool("correct", correct);

    }

    void OnCollisionEnter2D(Collision2D col)
    {
        if (col.collider.CompareTag("floor"))
        {
            floorTouched = true;
        }
    }


}
